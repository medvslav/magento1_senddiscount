# Magento1_Senddiscount
This is a module for Magento 1.x CMS.
Module for subscribing registered and unregistered customers to price change email.
You can enable your registered and unregistered customers to subscribe to price change email.

- When the module "Subscribe to the product discount" is enabled, a "I want to be informed if product will be discounted" link appears on every product page. 

- Customers can click the link to subscribe to emails related to the product.

- Whenever the price changes, or the product goes on special, everyone who has signed up to be notified receives an email about it.

- Eshop owner has in the table databse of customers, who are waiting for discount for particular product.

- The emails will be sended for customers automatically by Cron (default one time for day at 00:00) to all subsribed users and products which price has changed.

- When user get an email about price change of product, he can unsubscribe from email messages by clicking on link in the email (only for one product or all products subscription).

- When customer click on link "I want to be informed if product will be discounted", new form is loaded by javascript (the form is displayed on the same place without reloading a page).

Form has these input boxes:

- First name - input box - required

- Surname - input box - required

- Email - input box - required

- Phone - input box - optional

- Submit button "Send an emial, when product is discounted"

When customer send form, new messaage will be displayed, messaage must be managed in static html block in admin. The default text is "We are going to send you an email or give a call when this product will be discounted". 

- Send form will be done by AJAX (without reloading of page).


- The data that entered customers into the input fields in the form will be validated on the client and sever sides of the store.

Admin of Magento will have new view, under section "SALES", called "Potential Customer", table of standart layout with the ability filtering and sorting data. 
The table has columns:

- Date + Time

- IP address of customer

- First name

- Surname

- Email

- Phone

- SKU of product

- ID of product

- Status of subscription: pending, notified or cancelled

Admin user can manually change status or delete customer data from list

SKU, in the table, is link to product detail page

Admin can export the list of customers into CSV-, Excel-, XML-files.

Admin can manage some users for one time (change status or delete) through the "mass-actions". 

Installation of the extension needs to be done through Magento Connect in the administrative part of the site.

When you install this extension in the administrative part of the Magento will be created a new static CMS block - "For the message of module "Send Discount" " . In this block you can change the success message, which will be shown in the form on product page, when customer have subscribed to price change of the product. The default message is "We are going to send you an email or give a call when this product will be discounted".

To set up product alerts:

1. On the Admin menu, select System > Configuration. Then in the panel on the left, under
Catalog, select Catalog.

2. Click to expand the Product Alerts section, and do the following:

- To offer price change alerts to your customers, set Allow Alert When Product Price
Changes to "Yes".

- Set Price Alert Email Template to the template that you want to use for the price alert
notifications.
#